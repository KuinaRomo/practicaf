import random
import os.path

def readUserData():
    print("What is your name?")
    return input()

def printInstrucitions():
    print("__________________________________________________________________________" + '\n')
    print("load: Load a game from the actual user if the game exists")
    print("load [USERNAME]: Load a game form the user USERNAME if the game exists")
    print("save: Save the actual game")
    print("scores: Show the ranking of the scores of all saved games")
    print("help: Make apper this information")
    print("bye: Exit the game")
    print("__________________________________________________________________________" + '\n')


score = 0
gameOn = True
randomized = False
userData = readUserData()
triedTimes = 0
correctAnswers = 0

print("Welcome " + userData)
printInstrucitions()


def getUserAnswer(operation):
    print("\nWhat is " + str(num1) + " " + operation + " " + str(num2) + "?")
    return input()


def updateScores(incress):
    global score
    score = score + incress

def recoverInf():
    inf = ""
    if os.path.isfile("game.sav"):
        file = open("game.sav", 'r')
        inf = file.readlines()
    else:
        inf = "NO FILE"
    return inf


def saveGame():
    saved = False
    gamesSaved = recoverInf()

    if gamesSaved != "NO FILE":
        num = 0
        for game in gamesSaved:
            gameSav = game.split()
            if gameSav[0] == userData:
                gameSav[1] = str(score)
                gameSav[2] = str(triedTimes)
                gameSav[3] = str(correctAnswers) + '\n'
                gamesSaved[num] = " ".join(gameSav)
                saved = True
            num += 1

    if saved:
        newFile = open("game.sav", 'w')
        for game in gamesSaved:
            string = "".join(game)
            newFile.write(string)

    else:
        file = open("game.sav", 'a')
        strToSave = userData + " " + str(score) + " " + str(triedTimes) + " " + str(correctAnswers) + '\n'
        file.write(strToSave)


def loadGame(user):
    global score
    global triedTimes
    global correctAnswers
    global userData
    games = recoverInf()
    loaded = False
    for game in games:
        gameSav = game.split()
        if gameSav[0] == user:
            userData = user
            score = int(gameSav[1])
            triedTimes = int(gameSav[2])
            correctAnswers = int(gameSav[3])
            loaded = True

    if loaded:
        print("Game Loaded. Welcome " + userData + "!")

    else:
        print("There aren't any saved game of this user")


def randomOperation(num1, num2):
    global total
    randOp = random.randint(0, 3)
    if randOp == 0:
        total = num1 + num2
        return getUserAnswer('+').split()

    elif randOp == 1:
        total = num1 - num2
        return getUserAnswer('-').split()

    else:
        total = num1 * num2
        return getUserAnswer('*').split()


def maxInt(array):
    maxNum = array[0]
    for element in array:
        if element > maxNum:
            maxNum = element
    return maxNum

def ranking():
    savInf = recoverInf()
    if savInf == "NO FILE":
        print("There aren't any saved game yet")

    else:
        print("\n******* RANKING *******\n")
        allUsers = []
        allScores = []

        for savGames in savInf:
            allUsers.append(savGames.split()[0])
            allScores.append(savGames.split()[1])

        while allScores.__len__() > 0:
            maxScore = maxInt(allScores)
            position = 0

            for score in allScores:
                if score == maxScore:
                    print(allUsers[position] + " ---------------------- " + score)
                    del allUsers[position]
                    allScores.remove(score)
                    break
                position += 1


while gameOn:
    if randomized == False:
        num1 = random.randint(0, 100)
        num2 = random.randint(0, 100)
        randomized = True

    userAnswer = randomOperation(num1, num2)

    if userAnswer[0] == "bye":
        gameOn = False
        print("You answer correctly " + str(correctAnswers) + " answers of " + str(triedTimes) + " qüestions.")
        print("Scores: " + str(score))
        print("Goodbye " + userData + "!")

    elif userAnswer[0] == "save":
        print("The game has been saved")
        saveGame()

    elif userAnswer[0] == "load":
        if userAnswer.__len__() > 1:
          loadGame(userAnswer[1])

        else:
            loadGame(userData)

    elif userAnswer[0] == "help":
        printInstrucitions()

    elif userAnswer[0] == "scores":
        ranking()

    else:
        if str(total) == userAnswer[0]:
            print("Yes, you got it!")
            correctAnswers = correctAnswers + 1
            updateScores(2)

        else:
            print("No, sorry, the correct answer was: " + str(total))
            updateScores(-1)

        print("Your current score is: " + str(score) + '\n')
        triedTimes = triedTimes + 1
        randomized = False


