import pygame

class Bullet(pygame.sprite.Sprite):
    """Class to manage bullets fired from the ship"""

    def __init__(self, settings, screen, ship):
        pygame.sprite.Sprite.__init__(self)
        self.screen = screen

        self.rect = pygame.Rect(ship.rect.centerx, ship.rect.bottom,
                                settings.bullet_width, settings.bullet_height)

        self.rect.bottom = screen.get_rect().bottom

        self.ship = ship

    def draw(self):
        pygame.draw.rect(self.screen, (255, 0, 0), self.rect)

    def update(self):
        self.rect.bottom -= 1



